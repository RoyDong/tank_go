package model

import (
    "fmt"
    "time"
    "tank/app"
    "encoding/hex"
    "crypto/sha512"
)


func HashPasswd(passwd string, salt string) string {
    hash := sha512.New()

    if _, e := hash.Write([]byte(passwd + salt)); e != nil {
        return ""
    }

    return hex.EncodeToString(hash.Sum(nil))
}

type SUser struct {
    table string
    storage map[int64]*User
}

var UserShop = NewUserShop()

func NewUserShop() *SUser {
    s := &SUser{
        table: "user",
        storage: make(map[int64]*User, 1000),
    }

    go s.AutoClear()
    return s
}

func (s *SUser) Get(id int64) *User {
    user, has := s.storage[id]
    if has {
        return user
    }

    return s.FindBy("id", fmt.Sprintf("%d", id))
}

func (s *SUser) FindBy(k, v string) *User {
    user := new(User)
    stmt := fmt.Sprintf("select id,name,rank,email,passwd,salt,created_at,"+
            "updated_at,signin_at,distance,num_fire,num_kill,num_death,"+
            "num_battle,num_win from `%s` where `%s`='%s'", s.table, k, v)
    row := app.DB.QueryRow(stmt)
    e := row.Scan(&user.id, &user.name, &user.rank, &user.email, &user.passwd,
            &user.salt,&user.createdAt, &user.updatedAt, &user.signinAt,
            &user.distance, &user.numFire, &user.numKill, &user.numDeath,
            &user.numBattle, &user.numWin)
    if e != nil {
        return nil
    }

    user.session = nil

    if u, has := s.storage[user.id]; has {
        return u
    }

    s.storage[user.id] = user
    return user
}

func (s *SUser) Save(user *User) bool {
    data := user.dataForMysql()

    if user.id > 0 {
        if _, has := s.storage[user.id]; !has {
            s.storage[user.id] = user
        }
        return app.DB.Update(s.table, data, fmt.Sprintf("`id`=%d", user.id))
    }

    user.id = app.DB.Insert(s.table, data)

    if user.id == 0 {
        return false
    }

    s.storage[user.id] = user
    return true
}

func (s *SUser) Delete(id int64) {
    if app.DB.Delete(s.table, fmt.Sprintf("`id`=%d", id)) {
        delete(s.storage, id)
    }
}

func (s *SUser) AutoClear() {
    for _ = range time.Tick(60 * time.Second) {
        for _,u := range s.storage {
            if u.Session() == nil && u.Tank() == nil && u.Battle() == nil {
                delete(s.storage, u.Id())
            }
        }
    }
}
