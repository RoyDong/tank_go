package model

import (
    "dwlib"
    "time"
    "dwlib/animation"
)

const (
    QueueInitSize = 1000
    UserInitSize = 1000
    BattleInitSize = 100
)

type SBattle struct {
    storage map[int64]*Battle
    queue []chan *User
}

var BattleShop = NewBattleShop()

func NewBattleShop() *SBattle {
    q := []chan *User{
        make(chan *User, QueueInitSize),
        make(chan *User, QueueInitSize),
        make(chan *User, QueueInitSize),
    }

    s := &SBattle{
        storage: make(map[int64]*Battle, BattleInitSize),
        queue: q,
    }

    go s.MatchPlayers(1) //1v1
    go s.MatchPlayers(2) //2v2
    go s.MatchPlayers(3) //3v3

    return s
}

func (s *SBattle) New(id int64, size int) *Battle {
    b := &Battle{
        id:        id,
        createdAt: time.Now(),
        size:      size,
        players:   make([]*User, 0, size),
        scene:     animation.NewScene(),
    }

    s.storage[id] = b
    return b
}

func (s *SBattle) MatchPlayers(t int) {
    size := t * 2
    n := 0
    users := make(map[int64]*User, UserInitSize)
    queue := s.queue[t - 1]
    matched := make([]*User, 0, size)
    for {
        n = len(queue)
        for i := 0; i < n; i++ {
            u := <-queue
            users[u.Id()] = u
        }

        matched = matched[:0]
        for _,user := range users {
            if user.Session() != nil {
                if len(matched) < size {
                    matched = append(matched, user)
                } else {
                    rank := matched[0].Rank()
                    for i := 1; i < size; i++ {
                        if dwlib.Abs(user.Rank() - rank) <
                                dwlib.Abs(matched[i].Rank() - rank) {
                            matched[i] = user
                            break
                        }
                    }
                }
            }
        }

        if len(matched) < size {
            time.Sleep(time.Second * 5)
            continue
        }

        battle := s.New(matched[0].Id(), size)
        for _,user := range matched {
            battle.AddPlayer(user)
            delete(users, user.Id())
        }
        battle.AssignTeam()
        for _,u := range matched {
            if s := u.Session(); s != nil {
                s.Notify("battle_ready", []int64{battle.Id(), int64(u.Tag())})
            }
        }
    }
}

func (s *SBattle) AddPlayer(u *User, t int) {
    s.queue[t - 1] <- u
    u.SetState(UserStateMatching)
}

func (s *SBattle) Find(id int64) *Battle {
    b,_ := s.storage[id]

    return b
}

func (s *SBattle) All() []*BattleData {
    all := make([]*BattleData, 0, len(s.storage))

    for _, b := range s.storage {
        all = append(all, b.Data())
    }

    return all
}

func (s *SBattle) AllId() []int64 {
    all := make([]int64, 0, len(s.storage))

    for _, b := range s.storage {
        all = append(all, b.Id())
    }

    return all
}
