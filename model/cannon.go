package model

import (
    "math"
    "dwlib/animation"
)

type Cannon struct {
    *animation.Sprite
    carrier *MarkI
    chamber []*Bomb
    chamberSize int
    reloadTime, timeSinceLastReload float64
    bomb cannonBomb
}

type cannonBomb struct {
    speed, timeout, radius float64
}

func NewCannon(size int, r, s, t, rt float64) *Cannon {
    c := &Cannon{
        Sprite: animation.NewSprite(),
        chamberSize: size,
        chamber: make([]*Bomb, 0, size),
        reloadTime: rt,
        bomb: cannonBomb{
            speed: s,
            timeout: t,
            radius: r,
        },
    }

    for i := 0; i < size; i++ {
        c.Reload()
    }

    return c
}

func (c *Cannon) Carrier() *MarkI {
    return c.carrier
}

func (c *Cannon) SetCarrier(carrier *MarkI) {
    c.carrier = carrier
}

func (c *Cannon) Position() *animation.Vector {
    if c.carrier == nil {
        return c.Sprite.Position()
    }
    return c.carrier.Position()
}

func (c *Cannon) SetReloadTime(t float64) {
    c.reloadTime = t
}


func (c *Cannon) Launch(position *animation.Vector) bool {
    if len(c.chamber) == 0 {
        c.TriggerEvent("empty", c)
        return false
    }

    p := c.Position()
    w := position.X - p.X
    h := position.Y - p.Y
    s := math.Sqrt(w * w + h * h)
    r := math.Acos(w / s)
    if h < 0 {r = 2 * math.Pi - r}

    c.SetRotation(r)
    bomb := c.chamber[len(c.chamber) - 1]
    offset := c.Carrier().Radius() + bomb.Radius()
    bomb.SetPositionXY(p.X + offset * w / s, p.Y + offset * h / s)
    bomb.SetRotation(r)
    bomb.SetTag(c.Carrier().Tag())
    bomb.Fly(r)
    bomb.AddEventHandler("intersect", func(args ...interface{}){
        b := args[0].(*Bomb)
        sprites := args[1].([]animation.ISprite)
        bang := false
        tanks := make([]*MarkI, 0, len(sprites))
        for _,s := range sprites {
            if tank, ok := s.(*MarkI); ok && b.Tag() != tank.Tag() {
                tanks = append(tanks, tank)
                bang = true
            } else if bomb, ok := s.(*Bomb); ok && b.Tag() != bomb.Tag() {
                bomb.Destroy()
                bang = true
            }
        }
        if bang {
            b.Destroy()
        }
        if t := c.Carrier(); t != nil && len(tanks) > 0 {
            t.TriggerEvent("hit", t, tanks)
        }
    })
    c.Scene().AddSprite(bomb)
    c.chamber = c.chamber[:len(c.chamber) - 1]
    c.TriggerEvent("launch", c, bomb)
    return true
}

func (c *Cannon) Reload() {
    if len(c.chamber) < c.chamberSize {
        info := c.bomb
        bomb := NewBomb(info.radius, info.speed, info.timeout)
        bomb.SetLauncher(c)
        bomb.SetDisplay(true)
        c.chamber = append(c.chamber, bomb)
    }
}

func (c *Cannon) Update(dt float64) {
    c.timeSinceLastReload += dt

    if c.timeSinceLastReload >= c.reloadTime {
        c.Reload()
        c.timeSinceLastReload = 0
    }
}

func (c *Cannon) Destroy() {
    c.chamber = c.chamber[:0]
    c.Sprite.Destroy()
}

type CannonData struct {
    *animation.SpriteData
    ChamberSize int `json:"chamber_size"`
    ReloadTime float64 `json:"reload_time"`
    TimeSinceLastReload float64 `json:"time_since_last_reload"`
    Bomb cannonBombData `json:"bomb"`
}
type cannonBombData struct {
    Radius float64 `json:"radius"`
    Speed float64 `json:"speed"`
    Timtout float64 `json:"timeout"`
}

func (c *Cannon) Data() interface{} {
    s := c.Sprite.Data().(*animation.SpriteData)
    s.Sprite = "cannon"
    info := c.bomb

    return &CannonData{
        SpriteData: s,
        ChamberSize: c.chamberSize,
        ReloadTime: c.reloadTime,
        TimeSinceLastReload: c.timeSinceLastReload,
        Bomb: cannonBombData{
            Radius: info.radius,
            Speed: info.speed,
            Timtout: info.timeout,
        },
    }
}

type FireAction struct {
    Id int64 `json:"id"`
    X float64 `json:"x"`
    Y float64 `json:"y"`
}
