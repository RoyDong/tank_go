package model

import (
    "math"
    "dwlib/animation"
)

type Tank struct {
    *animation.Sprite

    destPosition *animation.Vector
    speed float64
    state int

    driver *User
    cannon *Cannon
    hpLimit int64
    hp int64
    respawnTime int64
    deathTime float64
}

func NewTank(r float64) *Tank {
    t := &Tank{
        Sprite: animation.NewCircleSprite(r),
        destPosition: new(animation.Vector),
        state: animation.StateRun,
    }

    t.SetRoot(true)
    return t
}

func (t *Tank) State() int {
    return t.state
}

func (t *Tank) SetState(s int) {
    t.state = s
}

func (t *Tank) SetSpeed(speed float64) {
    t.speed = speed
}

func (t *Tank) Speed() float64 {
    return t.speed
}

func (t *Tank) SetDestPosition(p *animation.Vector) {
    t.destPosition = p
}

func (t *Tank) DestPosition() *animation.Vector {
    return t.destPosition
}

func (t *Tank) HpLimit() int64 {
    return t.hpLimit
}

func (t *Tank) SetHpLimit(hp int64) {
    t.hpLimit = hp
}

func (t *Tank) Hp() int64 {
    return t.hp
}

func (t *Tank) SetHp(hp int64) {
    t.hp = hp
}

func (t *Tank) Damage(hp int64) {
    t.hp -= hp

    if t.hp <= 0 {
        t.hp = 0
        t.SetVisible(false)
        t.deathTime = 0
        t.TriggerEvent("die", t)
    }
}

func (t *Tank) RespawnTime() int64{
    return t.respawnTime
}

func (t *Tank) SetRespawnTime(time int64) {
    t.respawnTime = time
}

func (t *Tank) SetCannon(c *Cannon) {
    t.cannon = c
    //c.SetCarrier(t)
}

func (t *Tank) SetScene(s *animation.Scene) {
    t.Sprite.SetScene(s)
    s.AddSprite(t.cannon)
}

func (t *Tank) SetDriver(u *User) {
    t.driver = u
    //u.SetTank(t)
}

func (t *Tank) Driver() *User {
    return t.driver
}

func (t *Tank) Cannon() *Cannon {
    return t.cannon
}

func (t *Tank) SetDisplay(ok bool) {
    t.cannon.SetDisplay(ok)
    t.Sprite.SetDisplay(ok)
}

func (t *Tank) Destroy() {
    t.cannon.Destroy()
    t.cannon = nil
    t.Sprite.Destroy()
}


func (t *Tank) Update(dt float64) {
    if t.Visible() == false {
        t.deathTime += dt

        if int64(t.deathTime) >= t.respawnTime {
            t.SetVisible(true)
            t.SetHp(t.hpLimit)
            t.TriggerEvent("respawn", t)
        }
    }

    if p := t.Position(); t.state == animation.StateRun && t.speed > 0 &&
            p.Equle(t.destPosition) == false {

        w := t.destPosition.X - p.X
        h := t.destPosition.Y - p.Y
        s := math.Sqrt(w * w + h * h)
        ds := t.speed * dt

        if s <= ds {
            t.SetPosition(t.destPosition)
            t.TriggerEvent("arrive", t)
        } else {
            cos := w / s
            radian := math.Acos(cos)
            if h < 0 {
                radian = 2 * math.Pi - math.Acos(cos)
            }
            if t.cannon != nil {
                t.cannon.SetRotation(t.cannon.Rotation() + radian - t.Rotation())
            }
            t.SetRotation(radian)
            t.SetPositionXY(p.X + ds * cos, p.Y + ds * h / s)
        }
    }
}

type TankData struct {
    *animation.SpriteData
    DestPosition *animation.Vector `json:"dest_position"`
    Speed float64 `json:"speed"`
    Cannon interface{} `json:"cannon"`
}

func (t *Tank) Data() interface{} {
    s := t.Sprite.Data().(*animation.SpriteData)
    s.Sprite = "tank"

    return &TankData{
        SpriteData: s,
        DestPosition: t.destPosition,
        Speed: t.speed,
        Cannon: t.cannon.Data(),
    }
}
