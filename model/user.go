package model

import (
    "strconv"
    "dwlib"
    "dwlib/server"
)

const (
    UserStateOffline  = 0
    UserStateOnline   = 1
    UserStateAfk      = 2
    UserStateMatching = 3
    UserStateMatched  = 4
    UserStateReady    = 5
    UserStateInBattle = 6
)

type User struct {
    id, rank int64
    createdAt, updatedAt, signinAt int64
    name, email, passwd, salt string

    session *server.Session
    battle *Battle
    tank *MarkI
    state int
    tag int

    distance float64
    numFire int64
    numKill int64
    numDeath int64
    numBattle int64
    numWin int64
}

func (user *User) Id() int64 {
    return user.id
}


func (user *User) Rank() int64 {
    return user.rank
}
func (user *User) SetRank(r int64) {
    user.rank = r
}
func (user *User) IncRank(r int64) {
    user.rank += r
    if user.rank < 0 {
        user. rank = 0
    }
}


func (user *User) State() int {
    return user.state
}
func (user *User) SetState(state int) {
    user.state = state
}


func (user *User) Name() string {
    return user.name
}
func (user *User) SetName(name string) {
    user.name = name
}


func (user *User) Email() string {
    return user.email
}
func (user *User) SetEmail(email string) {
    user.email = email
}


func (user *User) CreatedAt() int64 {
    return user.createdAt
}
func (user *User) SetCreatedAt(t int64) {
    user.createdAt = t
}


func (user *User) UpdatedAt() int64 {
    return user.updatedAt
}
func (user *User) SetUpdatedAt(t int64) {
    user.updatedAt = t
}


func (user *User) SigninAt() int64 {
    return user.signinAt
}
func (user *User) SetSigninAt(t int64) {
    user.signinAt = t
}


func (user *User) Distance() float64 {
    return user.distance
}
func (user *User) SetDistance(d float64) {
    user.distance = d
}
func (user *User) IncDistance(d float64) {
    user.distance += d
}


func (user *User) NumFire() int64 {
    return user.numFire
}
func (user *User) SetNumFire(n int64) {
    user.numFire = n
}
func (user *User) IncNumFire(n int64) {
    user.numFire += n
}


func (user *User) NumKill() int64 {
    return user.numKill
}
func (user *User) SetNumKill(n int64) {
    user.numKill = n
}
func (user *User) IncNumKill(n int64) {
    user.numKill += n
}


func (user *User) NumDeath() int64 {
    return user.numDeath
}
func (user *User) SetNumDeath(n int64) {
    user.numDeath = n
}
func (user *User) IncNumDeath(n int64) {
    user.numDeath += n
}


func (user *User) NumBattle() int64 {
    return user.numBattle
}
func (user *User) SetNumBattle(n int64) {
    user.numBattle = n
}
func (user *User) IncNumBattle(n int64) {
    user.numBattle += n
}


func (user *User) NumWin() int64 {
    return user.numWin
}
func (user *User) SetNumWin(n int64) {
    user.numWin = n
}
func (user *User) IncNumWin(n int64) {
    user.numWin += n
}


func (user *User) Tag() int {
    return user.tag
}
func (user *User) SetTag(t int) {
    user.tag = t
}


func (user *User) Tank() *MarkI {
    return user.tank
}
func (user *User) SetTank(tank *MarkI) {
    user.tank = tank
}


func (user *User) Battle() *Battle {
    return user.battle
}
func (user *User) SetBattle(battle *Battle) {
    user.battle = battle
}


func (user *User) Upgrade(avarage int64) {
    delta := BattleScore - (user.rank - avarage) / BattleScore
    if delta > 0 {
        user.rank += delta
    }
}
func (user *User) Degrade(avarage int64) {
    delta := BattleScore + (user.rank - avarage) / BattleScore
    if delta > 0 {
        user.rank -= delta
        if user.rank < 0 {
            user.rank = 0
        }
    }
}


func (user *User) Session() *server.Session {
    if user.session != nil && user.session.IsOpen() {
        return user.session
    }
    user.session = nil
    return nil
}
func (user *User) SetSession(s *server.Session) {
    user.session = s
    if s := user.State(); s == UserStateOffline || s == UserStateAfk {
        user.SetState(UserStateOnline)
    }
}


func (user *User) SetPasswd(passwd string) {
    user.salt = dwlib.RandString(32)
    user.passwd = HashPasswd(passwd, user.salt)
}
func (user *User) CheckPasswd(passwd string) bool {
    return HashPasswd(passwd, user.salt) == user.passwd
}


type UserData struct {
    Id int64 `json:"id"`
    Name string `json:"name"`
    Email string `json:"email"`
    Rank int64 `json:"rank"`
    CreatedAt int64 `json:"created_at"`
    Tag int `json:"tag"`
    State int `json:"state"`
    Tid int64 `json:"tid"`
}

func (user *User) Data() *UserData {
    var tid int64
    if user.Tank() != nil {
        tid = user.Tank().Id()
    }
    return &UserData{
        Id: user.id,
        Name: user.name,
        Email: user.email,
        Rank: user.rank,
        CreatedAt: user.createdAt,
        Tag: user.tag,
        State: user.state,
        Tid: tid,
    }
}

func (user *User) dataForMysql() map[string]string {
    return map[string]string{
        "name": user.name,
        "email": user.email,
        "passwd": user.passwd,
        "salt": user.salt,
        "rank": strconv.FormatInt(user.rank, 10),
        "created_at": strconv.FormatInt(user.createdAt, 10),
        "updated_at": strconv.FormatInt(user.updatedAt, 10),
        "signin_at": strconv.FormatInt(user.signinAt, 10),
        "distance": strconv.FormatInt(int64(user.distance), 10),
        "num_fire": strconv.FormatInt(user.numFire, 10),
        "num_kill": strconv.FormatInt(user.numKill, 10),
        "num_death": strconv.FormatInt(user.numDeath, 10),
        "num_battle": strconv.FormatInt(user.numBattle, 10),
        "num_win": strconv.FormatInt(user.numWin, 10),
    }
}
