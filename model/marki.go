package model

import (
    "math"
    "dwlib/animation"
)

const (
    TankStateStop = 0
    TankStateRun = 1
    TankStateDie = 2
)

type MarkI struct {
    *animation.Sprite

    direction *animation.Vector
    destRotation, leg, sin, cos float64
    speed float64
    state int

    driver *User
    cannon *Cannon
    hpLimit int64
    hp int64
    respawnTime int64
    deathTime float64
}

func NewMarkI(r float64) *MarkI {
    t := &MarkI{
        Sprite: animation.NewCircleSprite(r),
        direction: new(animation.Vector),
        state: TankStateRun,
    }

    t.SetRoot(true)
    return t
}

func (t *MarkI) State() int {
    return t.state
}

func (t *MarkI) SetState(s int) {
    t.state = s
}

func (t *MarkI) SetSpeed(speed float64) {
    t.speed = speed
}

func (t *MarkI) Speed() float64 {
    return t.speed
}

func (t *MarkI) SetDirection(d *animation.Vector) {
    t.leg = math.Sqrt(d.X * d.X + d.Y * d.Y)
    t.cos = d.X / t.leg
    t.sin = d.Y / t.leg
    if d.Y < 0 {
        t.destRotation = math.Pi * 2 - math.Acos(t.cos)
    } else {
        t.destRotation = math.Acos(t.cos)
    }
    t.direction = d
    t.state = TankStateRun
}

func (t *MarkI) Direction() *animation.Vector {
    return t.direction
}

func (t *MarkI) HpLimit() int64 {
    return t.hpLimit
}

func (t *MarkI) SetHpLimit(hp int64) {
    t.hpLimit = hp
}

func (t *MarkI) Hp() int64 {
    return t.hp
}

func (t *MarkI) SetHp(hp int64) {
    t.hp = hp
}

func (t *MarkI) Damage(hp int64) {
    t.hp -= hp

    if t.hp <= 0 {
        t.hp = 0
        t.SetVisible(false)
        t.deathTime = 0
        t.SetState(TankStateDie)
        t.TriggerEvent("die", t)
    }
}

func (t *MarkI) RespawnTime() int64{
    return t.respawnTime
}

func (t *MarkI) SetRespawnTime(time int64) {
    t.respawnTime = time
}

func (t *MarkI) SetCannon(c *Cannon) {
    t.cannon = c
    c.SetCarrier(t)
}

func (t *MarkI) SetScene(s *animation.Scene) {
    t.Sprite.SetScene(s)
    s.AddSprite(t.cannon)
}

func (t *MarkI) SetDriver(u *User) {
    t.driver = u
    u.SetTank(t)
}

func (t *MarkI) Driver() *User {
    return t.driver
}

func (t *MarkI) Cannon() *Cannon {
    return t.cannon
}

func (t *MarkI) SetDisplay(ok bool) {
    t.cannon.SetDisplay(ok)
    t.Sprite.SetDisplay(ok)
}

func (t *MarkI) Destroy() {
    t.cannon.Destroy()
    t.cannon = nil
    t.Sprite.Destroy()
    t.ClearAllEventHandlers()
}


func (t *MarkI) Update(dt float64) {
    if t.State() == TankStateDie {
        t.deathTime += dt
        return
    }

    if t.state == TankStateStop || t.speed <= 0 {
        return
    }

    r := t.destRotation - t.Rotation()
    dr := t.RotateSpeed() * dt
    lastRotation := t.Rotation()

    if r < -math.Pi {
        t.SetRotation(lastRotation + dr)
        r += 2 * math.Pi
    } else if r < 0 {
        t.SetRotation(lastRotation - dr)
        r = -r
    } else if r <= math.Pi {
        t.SetRotation(lastRotation + dr)
    } else {
        r = 2 * math.Pi -r
        t.SetRotation(lastRotation - dr)
    }

    if dr > r || r < math.Pi / 9 || t.RotateSpeed() <= 0 {
        ds := dt * t.speed
        p := t.Position()
        t.SetRotation(t.destRotation)
        t.SetPositionXY(p.X + ds * t.cos, p.Y + ds * t.sin)
    }

    if c := t.cannon; c != nil {
        c.SetRotation(c.Rotation() + t.Rotation() - lastRotation)
    }
}

type MarkIData struct {
    *animation.SpriteData
    Direction *animation.Vector `json:"direction"`
    Speed float64 `json:"speed"`
    Cannon interface{} `json:"cannon"`
}

func (t *MarkI) Data() interface{} {
    s := t.Sprite.Data().(*animation.SpriteData)
    s.Sprite = "tank"

    return &MarkIData{
        SpriteData: s,
        Direction: t.direction,
        Speed: t.speed,
        Cannon: t.cannon.Data(),
    }
}
