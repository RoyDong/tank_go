package model

import (
    "math"
    "sort"
    "time"
    "dwlib/animation"
)

const (
    BattleStateReady = 0
    BattleStateBegin = 1
    BattleStateEnd   = 2
    BattleScore      = int64(10)
    RemoraTag        = 100
)

type Battle struct {
    id int64
    size int
    state int
    createdAt time.Time
    players []*User
    teams []*Team
    scene *animation.Scene
    rank int64
}

func (b *Battle) Id() int64 {
    return b.id
}


func (b *Battle) State() int {
    return b.state
}
func (b *Battle) SetState(s int) {
    b.state = s
}


func (b *Battle) AddPlayer(u *User) {
    if len(b.players) < b.size {
        b.players = append(b.players, u)
        u.SetBattle(b)
        u.SetState(UserStateMatched)
        b.rank += u.Rank()
    }
}
func (b *Battle) AssignTeam() {
    size := b.size / 2
    b.teams = make([]*Team, 0, 2)
    for i := 0; i < 2; i++ {
        team := &Team{tag: i, size: size}
        b.teams = append(b.teams, team)
    }

    sort.Sort(b)
    for i, u := range b.players {
        b.teams[i % 2].AddPlayer(u)
    }
}

func (b *Battle) AvarageRank() int64 {
    return b.rank / int64(len(b.players))
}

func (b *Battle) SetRanks(loseTag int) {
    avarage := b.AvarageRank()

    for _,u := range b.players {
        if u.Tag() == loseTag {
            u.Degrade(avarage)
        } else {
            u.Upgrade(avarage)
        }

        UserShop.Save(u)
    }
}

func (b *Battle) CheckReady() {
    n := 0
    for _,u := range b.players {
        if u.State() == UserStateReady {
            n++
        }
    }

    if n >= b.size {
        b.InitScene()
        b.SetState(BattleStateBegin)
        b.Broadcast("battle_begins", nil)
    }
}

func (b *Battle) InitScene() {
    for _,u := range b.players {
        b.BuildPlayerTank(u)
    }

    northWall := animation.NewRectSprite(700, 1)
    northWall.SetPositionXY(-10, 640)
    northWall.SetTag(RemoraTag)
    northWall.SetDisplay(true)
    b.scene.AddSprite(northWall)

    southWall := animation.NewRectSprite(700, 1)
    southWall.SetPositionXY(-10, 1)
    southWall.SetTag(RemoraTag)
    southWall.SetDisplay(true)
    b.scene.AddSprite(southWall)

    eastWall := animation.NewRectSprite(400, 1)
    eastWall.SetPositionXY(320, -10)
    eastWall.SetTag(RemoraTag)
    eastWall.SetDisplay(true)
    b.scene.AddSprite(eastWall)

    westWall := animation.NewRectSprite(400, 1)
    westWall.SetPositionXY(1, -10)
    westWall.SetTag(RemoraTag)
    westWall.SetDisplay(true)
    b.scene.AddSprite(westWall)

    go b.scene.Start()

    b.scene.AddEventHandler("check", func(args ...interface{}) {
        for _,u := range b.players {
            if t := u.Tank(); u.Session() == nil && t.State() != TankStateDie {
                t.SetState(TankStateDie)
                b.TankDied(t)
            }
        }
    })
}

func (b *Battle) TankDied(tank *MarkI) {
    b.Broadcast("die", []int64{tank.Id()})

    for _,u := range b.players {
        if t := u.Tank(); t.Tag() == tank.Tag() && t.State() != TankStateDie {
            return
        }
    }

    b.Broadcast("battle_end", []int{tank.Tag()})
    b.SetRanks(tank.Tag())
    b.Destroy()
}

func (b *Battle) BuildPlayerTank(u *User) {
    tank := NewMarkI(13)
    tank.SetTag(u.Tag())
    tank.SetCannon(NewCannon(3, 5, 300, 0.6, 0.5))
    tank.SetSpeed(100)
    tank.SetRotateSpeed(0 * math.Pi)
    tank.SetDisplay(true)
    tank.SetHpLimit(5)
    tank.SetHp(5)
    tank.SetRespawnTime(3)
    tank.SetDriver(u)

    if tank.Tag() == 0 {
        tank.SetPositionXY(160, 150)
    } else {
        tank.SetPositionXY(160, 400)
    }

    tank.AddEventHandler("die", func(args ...interface{}) {
        b.TankDied(args[0].(*MarkI))
    })

    tank.AddEventHandler("respawn", func(args ...interface{}) {
        me := args[0].(*MarkI)
        b.Broadcast("respawn", []int64{me.Id()})
    })

    tank.AddEventHandler("hit", func(args ...interface{}) {
        tanks := args[1].([]*MarkI)
        for _,t := range tanks {
            t.Damage(1)
        }
    })

    tank.AddEventHandler("intersect", func(args ...interface{}) {
        me := args[0].(*MarkI)
        me.SetState(TankStateStop)
    })

    b.scene.AddSprite(tank)
    u.SetState(UserStateInBattle)

    if s := u.Session(); s != nil {
        u.Session().Notify("user_tank", tank.Id())
    }
}


func (b *Battle) Scene() *animation.Scene {
    return b.scene
}

func (b *Battle) Destroy() {
    delete(BattleShop.storage, b.id)
    for _,u := range b.players {
        u.SetTank(nil)
        u.SetBattle(nil)
        u.SetState(UserStateOnline)
    }
    b.players = b.players[:0]
    b.scene.Destroy()
    b.scene = nil
    b.SetState(BattleStateEnd)
}


func (b *Battle) Len() int {
    return len(b.players)
}
func (b *Battle) Less(i, j int) bool {
    return b.players[i].Rank() > b.players[j].Rank()
}
func (b *Battle) Swap(i, j int) {
    b.players[i], b.players[j] = b.players[j], b.players[i]
}


type BattleData struct {
    Id int64 `json:"id"`
    CreatedAt int64 `json:"created_at"`
    State int `json:"state"`
    Players []*UserData `json:"players"`
}

func (b *Battle) Data() *BattleData {
    d := &BattleData{
        Id: b.id,
        CreatedAt: b.createdAt.Unix(),
        State: b.state,
        Players: make([]*UserData, 0),
    }

    for _,u := range b.players {
        d.Players = append(d.Players, u.Data())
    }

    return d
}

func (b *Battle) Broadcast(name string, data interface{}) {
    for _,u := range b.players {
        if s := u.Session(); s != nil {
            s.Notify(name, data)
        }
    }
}
