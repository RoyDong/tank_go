package model



type Team struct {
    players []*User
    tag int
    rank int64
    size int
}

func (t *Team) AddPlayer(u *User) {
    if len(t.players) < t.size {
        u.SetTag(t.tag)
        t.players = append(t.players, u)
        t.rank += u.Rank()
    }
}

func (t *Team) AvarageRank() int64 {
    return t.rank / int64(len(t.players))
}

func (t *Team) TotalRank() int64 {
    return t.rank
}

func (t *Team) Tag() int {
    return t.tag
}

func (t *Team) Broadcast(name string, data interface{}) {
    for _,u := range t.players {
        if s := u.Session(); s != nil {
            s.Notify(name, data)
        }
    }
}

func (t *Team) Data() *TeamData {
    return &TeamData{
        Tag: t.tag,
        AvarageRank: t.AvarageRank(),
        TotalRank: t.TotalRank(),
    }
}

type TeamData struct {
    Tag int `json:"tag"`
    AvarageRank int64 `json:"avarage_rank"`
    TotalRank int64 `json:"tota_rank"`
    Size int `json:"size"`
}
