package model

import (
    "math"
    "dwlib/animation"
)

type Bomb struct {
    *animation.Sprite
    launcher *Cannon
    speed, flyTime, timeout float64
}


func NewBomb(r, s, t float64) *Bomb {
    return &Bomb{
        Sprite: animation.NewCircleSprite(r),
        speed: s,
        timeout: t,
    }
}

func (b *Bomb) SetSpeed(s float64) {
    b.speed = s
}

func (b *Bomb) Timeout() float64 {
    return b.timeout
}

func (b *Bomb) SetTimeout(t float64) {
    b.timeout = t
}

func (b *Bomb) Launcher() *Cannon {
    return b.launcher
}

func (b *Bomb) SetLauncher(c *Cannon) {
    b.launcher = c
}

func (b *Bomb) Fly(angle float64) {
    b.SetVelocityXY(b.speed * math.Cos(angle), b.speed * math.Sin(angle))
}

func (b *Bomb) Update(dt float64) {
    if b.flyTime += dt; b.flyTime > b.timeout {
        b.Destroy()
    } else {
        b.Sprite.Update(dt)
    }
}

type BombData struct {
    *animation.SpriteData
    Speed float64 `json:"speed"`
    FlyTime float64 `json:"fly_time"`
    Timeout float64 `json:"timeout"`
}

func (b *Bomb) Data() interface{} {
    s := b.Sprite.Data().(*animation.SpriteData)
    s.Sprite = "bomb"
    s.CollisionType = animation.CollisionTypeCircle

    return &BombData{
        SpriteData: s,
        Speed: b.speed,
        FlyTime: b.flyTime,
        Timeout: b.timeout,
    }
}
