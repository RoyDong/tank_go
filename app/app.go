package app

import (
    "log"
    "strings"
    "dwlib"
    "dwlib/mysql"
)

const (
    ConfigSearchPath = ".:/etc/tank"
    ConfigFilename = "config.json"
)

type config struct {
    Name string `json:"name"`
    Version string `json:"version"`
    LogFile string `json:"log_file"`
    Env string `json:"env"`
    Port int `json:"port"`
    Mysql struct{
        Dsn string `json:"dsn"`
        Dbname string `json:"dbname"`
    } `json:"mysql"`
}

var Config = new(config)
var DB *mysql.DB

func Init() {
    configLoaded := false
    for _,path := range strings.Split(ConfigSearchPath, ":") {
        file := strings.TrimRight(path, "/") + "/" + ConfigFilename

        if e := dwlib.LoadJson(Config, file); e == nil {
            configLoaded = true
            break
        }
    }

    if !configLoaded {
        log.Fatal("Config file not found")
    }

    DB = mysql.NewDB(Config.Mysql.Dsn, Config.Mysql.Dbname)
}
