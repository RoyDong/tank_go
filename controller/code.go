package controller


const (

    MissParam = 1001
    ParamError = 1002
    ParamFormatError = 1003

    NotFound = 1004
    ServerError = 1005

    NoPermission = 1006
)
