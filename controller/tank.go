package controller

import (
    "dwlib/server"
    "dwlib/animation"
    "tank/model"
)


type PointAction struct {
    Id int64 `json:"id"`
    X float64 `json:"x"`
    Y float64 `json:"y"`
}

type Tank struct {
    *server.Message
    user *model.User
}

func (c *Tank) Init() {
    CheckUserSession(c, &c.user)
}

func (c *Tank) Move() {
    var x, y float64
    var has bool
    if x, has = c.GetFloat("x"); !has {
        c.Panic(MissParam, "miss x")
    }

    if y, has = c.GetFloat("y"); !has {
        c.Panic(MissParam, "miss y")
    }

    tank := c.user.Tank()
    if tank.Visible() == false {
        c.Panic(NoPermission, "you are dead")
    }

    move := &PointAction{Id: tank.Id(), X: x, Y: y}
    tank.SetDirection(&animation.Vector{X: x, Y: y})
    c.user.Battle().Broadcast("move", move)
}

func (c *Tank) Fire() {
    u := c.Session().Get("user").(*model.User)
    if u == nil {
        c.Panic(NoPermission, "You have not signin")
    }

    var x, y float64
    var has bool
    if x, has = c.GetFloat("x"); !has {
        c.Panic(MissParam, "miss x")
    }

    if y, has = c.GetFloat("y"); !has {
        c.Panic(MissParam, "miss y")
    }

    tank := c.user.Tank()
    if tank.Visible() == false {
        c.Panic(NoPermission, "you are dead")
    }
    if tank.Cannon().Launch(&animation.Vector{X: x, Y: y}) {
        fire := &PointAction{Id: tank.Id(), X: x, Y: y}
        c.user.Battle().Broadcast("fire", fire)
    }
}

func (c *Tank) Stop() {
    u := c.Session().Get("user").(*model.User)
    if u == nil {
        c.Panic(NoPermission, "You have not signin")
    }

    tank := c.user.Tank()
    if tank == nil || tank.Visible() == false {
        c.Panic(NoPermission, "you are dead")
    }
    tank.SetState(model.TankStateStop)
    c.user.Battle().Broadcast("stop", []int64{tank.Id()})
}
