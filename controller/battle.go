package controller

import (
    "dwlib/server"
    "tank/model"
)


type Battle struct {
    *server.Message
    user *model.User
}

func (c *Battle) Init() {
    CheckUserSession(c, &c.user)
}

func (c *Battle) Current() {
    battle := c.user.Battle()
    if battle == nil {
        c.Panic(NoPermission, "You are not in a battle")
    }

    c.ReplySuccess(battle.Data())
}

func (c *Battle) Match() {
    if c.user.Battle() != nil {
        c.Panic(NoPermission, "You are allready in a battle")
    }

    t, has := c.GetInt("type")
    if !has {
        t = 1
    }

    model.BattleShop.AddPlayer(c.user, int(t))
    c.ReplySuccess(nil)
}

func (c *Battle) Ready() {
    if battle := c.user.Battle(); battle != nil &&
            battle.State() == model.BattleStateReady &&
            c.user.State() == model.UserStateMatched {

        c.user.SetState(model.UserStateReady)
        battle.CheckReady()
    }
}

func (c *Battle) Cancle() {
    if battle := c.user.Battle(); battle != nil &&
            battle.State() == model.BattleStateReady &&
            c.user.State() == model.UserStateMatched {

        battle.Destroy()
    }
    c.ReplySuccess(nil)
}

func (c *Battle) Scene() {
    battle := c.user.Battle()
    if battle == nil {
        c.Panic(NoPermission, "You must join a battle first")
    }

    c.ReplySuccess(battle.Scene().Data())
}

