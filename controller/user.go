package controller

import (
    "time"
    "regexp"
    "dwlib/server"
    "tank/model"
)

type User struct {
    *server.Message
}

func (c *User) Info() {
    var id int64
    var has bool
    if id, has = c.GetInt("id"); !has {
        c.Panic(MissParam, "miss param id")
    }
    if user := model.UserShop.Get(id); user != nil {
        c.ReplySuccess(user.Data())
    } else {
        c.Panic(NotFound, "User not found")
    }
}

func (c *User) Current() {
    var user *model.User
    CheckUserSession(c, &user)
    c.ReplySuccess(user.Data())
}

func (c *User) Signin() {
    var user *model.User
    var email, passwd string
    var has bool

    if email, has = c.Get("email"); !has || len(email) == 0 {
        c.Panic(MissParam, "Miss param email")
    }
    if passwd, has = c.Get("passwd"); !has || len(passwd) < 6 {
        c.Panic(ParamFormatError, "Password must more than 6")
    }

    ushop := model.UserShop
    if user = ushop.FindBy("email", email); user == nil {
        c.Panic(ParamError, "Email not exists")
    }
    if !user.CheckPasswd(passwd) {
        c.Panic(ParamError, "Password error")
    }

    user.SetSession(c.Session())
    user.SetSigninAt(time.Now().Unix())
    c.Session().Set("user", user)
    ushop.Save(user)
    c.ReplySuccess(user.Data())
}

func (c *User) Signup() {
    var user *model.User
    var email, passwd string
    var has bool
    if email, has = c.Get("email"); !has || len(email) == 0 {
        c.Panic(MissParam, "miss param email")
    }

    if passwd, has = c.Get("passwd"); !has || len(passwd) < 1 {
        c.Panic(ParamFormatError, "password must more than 6")
    }

    r := regexp.MustCompile(`^[^@]+@[^@]+$`)
    if r.Match([]byte(email)) == false {
        c.Panic(ParamError, "Email not right")
    }

    ushop := model.UserShop
    if ushop.FindBy("email", email) != nil {
        c.Panic(ParamError, "Email exists")
    }

    t := time.Now().Unix()
    r = regexp.MustCompile(`@.*$`)
    user = new(model.User)
    user.SetEmail(email)
    user.SetName(string(r.ReplaceAll([]byte(email), nil)))
    user.SetPasswd(passwd)
    user.SetCreatedAt(t)
    user.SetUpdatedAt(t)
    user.SetSigninAt(t)

    if ushop.Save(user) {
        user.SetSession(c.Session())
        c.Session().Set("user", user)
        c.ReplySuccess(user.Data())
    } else {
        c.Panic(ServerError, "mysql db error")
    }
}

func (c *User) SigninOrSignup() {
    var user *model.User
    var email, passwd string
    var has bool

    if email, has = c.Get("email"); !has || len(email) == 0 {
        c.Panic(MissParam, "miss param email")
    }

    if passwd, has = c.Get("passwd"); !has || len(passwd) < 1 {
        c.Panic(ParamFormatError, "password must more than 6")
    }

    ushop := model.UserShop
    t := time.Now().Unix()

    if user = ushop.FindBy("email", email); user == nil {
        r := regexp.MustCompile(`@.*$`)
        user = new(model.User)
        user.SetEmail(email)
        user.SetName(string(r.ReplaceAll([]byte(email), nil)))
        user.SetPasswd(passwd)
        user.SetCreatedAt(t)
        user.SetUpdatedAt(t)
        user.SetSigninAt(t)

        if ushop.Save(user) {
            user.SetSession(c.Session())
            c.Session().Set("user", user)
            c.ReplySuccess(user.Data())
        } else {
            c.Panic(ServerError, "mysql error")
        }
        return
    }

    if !user.CheckPasswd(passwd) {
        c.Panic(ParamError, "passwd error")
    }

    user.SetSession(c.Session())
    user.SetSigninAt(t)
    c.Session().Set("user", user)
    ushop.Save(user)
    c.ReplySuccess(user.Data())
}
