package controller

import (
    "dwlib/server"
    "tank/model"
)

func CheckUserSession(m server.IMessage, p **model.User) {
    if u := m.Session().Get("user"); u != nil {
        if *p = u.(*model.User); *p != nil {
            return
        }
    }
    m.Panic(NoPermission, "You have not signin")
}
