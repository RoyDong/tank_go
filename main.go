package main

import (
    "dwlib/server"
    "tank/app"
    "tank/controller"
)

func init() {
    app.Init()

    if app.Config.Env == "dev" {
        server.Debug = true
    }

    server.InitLogger(app.Config.LogFile)
    server.RegController(new(controller.User))
    server.RegController(new(controller.Tank))
    server.RegController(new(controller.Battle))
}

func main() {
    server.Listen(app.Config.Port)
}
