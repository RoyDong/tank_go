DROP TABLE `user` IF EXISTS;

CREATE TABLE `user` (
      `id` bigint(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
      `salt` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
      `passwd` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
      `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
      `rank` int(11) unsigned NOT NULL DEFAULT '0',
      `created_at` bigint(20) unsigned NOT NULL DEFAULT '0',
      `updated_at` bigint(20) unsigned NOT NULL DEFAULT '0',
      `signin_at` bigint(20) unsigned NOT NULL DEFAULT '0',
      `distance` bigint(20) unsigned NOT NULL DEFAULT '0',
      `num_fire` bigint(20) unsigned NOT NULL DEFAULT '0',
      `num_kill` bigint(20) unsigned NOT NULL DEFAULT '0',
      `num_death` bigint(20) unsigned NOT NULL DEFAULT '0',
      `num_battle` bigint(20) unsigned NOT NULL DEFAULT '0',
      `num_win` bigint(20) unsigned NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`),
      UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
